package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.seminario.cientificos.exception.ObjetoNuloException;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;

public class Professor implements DataValidation {

    private Long id;
    private String email;
    private String nome;
    private Double salario;
    private String telefone;

    private List<Seminario> seminarios = new ArrayList<>();

    private Instituicao instituicao;

    public Professor(Instituicao instituicao) {
        this.instituicao = instituicao;
    }

    public void adicionarSeminario(Seminario seminario) {
        if (Boolean.FALSE.equals(this.possuiSeminario(seminario))) {
            this.seminarios.add(seminario);
        }
    }

    public Boolean possuiSeminario(Seminario seminario) {
        return this.seminarios.contains(seminario);
    }

    @Override
    public void validateForDataModification() {
        if (StringUtils.isBlank(this.email) || this.email.length() > 50) {
            throw new SeminariosCientificosException("ER0060");
        }

        if (StringUtils.isBlank(this.nome) || this.nome.length() > 50) {
            throw new SeminariosCientificosException("ER0061");
        }

        if (StringUtils.isBlank(this.telefone) || this.telefone.length() > 15) {
            throw new SeminariosCientificosException("ER0062");
        }

        if (this.salario == null || this.salario <= 0) {
            throw new SeminariosCientificosException("ER0063");
        }

        if (this.instituicao == null) {
            throw new ObjetoNuloException();
        }

        this.instituicao.validateForDataModification();
    }

    public Long getId() {
        return this.id;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getSalario() {
        return this.salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public List<Seminario> getSeminarios() {
        return this.seminarios;
    }

    public Instituicao getInstituicao() {
        return this.instituicao;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Professor other = (Professor) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return true;
    }
}
