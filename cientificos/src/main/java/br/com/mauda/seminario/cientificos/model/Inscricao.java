package br.com.mauda.seminario.cientificos.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import br.com.mauda.seminario.cientificos.exception.ObjetoNuloException;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class Inscricao implements DataValidation {

    private Long id;

    private Boolean direitoMaterial;

    private LocalDateTime dataCriacao;

    private LocalDateTime dataCompra;

    private LocalDateTime dataCheckIn;

    private Seminario seminario;

    private Estudante estudante;

    private SituacaoInscricaoEnum situacao = SituacaoInscricaoEnum.DISPONIVEL;

    public Inscricao(Seminario seminario) {
        this.seminario = seminario;
        this.seminario.adicionarInscricao(this);
        this.dataCriacao = LocalDateTime.now();
    }

    public void comprar(Estudante estudante, Boolean direitoMaterial) {
        if (this.situacao.equals(SituacaoInscricaoEnum.DISPONIVEL) && LocalDate.now().isBefore(this.seminario.getData())) {
            this.estudante = estudante;
            estudante.adicionarInscricao(this);
            this.direitoMaterial = direitoMaterial;
            this.situacao = SituacaoInscricaoEnum.COMPRADO;
            this.dataCompra = LocalDateTime.now();
        }
    }

    public void cancelarCompra() {
        this.estudante.removerInscricao(this);
        this.estudante = null;
        this.direitoMaterial = null;
        this.situacao = SituacaoInscricaoEnum.DISPONIVEL;
        this.dataCompra = null;
    }

    public void realizarCheckIn() {
        this.situacao = SituacaoInscricaoEnum.CHECKIN;
        this.dataCheckIn = LocalDateTime.now();
    }

    @Override
    public void validateForDataModification() {
        if (this.situacao == null) {
            throw new SeminariosCientificosException("ER0040");
        }

        if (this.seminario == null) {
            throw new ObjetoNuloException();
        }

        this.seminario.validateForDataModification();

        if (!SituacaoInscricaoEnum.DISPONIVEL.equals(this.situacao)) {
            if (this.direitoMaterial == null) {
                throw new SeminariosCientificosException("ER0041");
            }
            if (this.estudante == null) {
                throw new ObjetoNuloException();
            }
            this.estudante.validateForDataModification();
        }
    }

    public Long getId() {
        return this.id;
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public void setDireitoMaterial(Boolean direitoMaterial) {
        this.direitoMaterial = direitoMaterial;
    }

    public LocalDateTime getDataCriacao() {
        return this.dataCriacao;
    }

    public LocalDateTime getDataCompra() {
        return this.dataCompra;
    }

    public LocalDateTime getDataCheckIn() {
        return this.dataCheckIn;
    }

    public Seminario getSeminario() {
        return this.seminario;
    }

    public Estudante getEstudante() {
        return this.estudante;
    }

    public SituacaoInscricaoEnum getSituacao() {
        return this.situacao;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Inscricao other = (Inscricao) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return true;
    }
}
