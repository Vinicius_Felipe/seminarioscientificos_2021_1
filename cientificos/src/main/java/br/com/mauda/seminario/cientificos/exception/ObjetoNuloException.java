package br.com.mauda.seminario.cientificos.exception;

public class ObjetoNuloException extends SeminariosCientificosException {

    private static final long serialVersionUID = -8170604861599008253L;

    public ObjetoNuloException() {
        super("ER0003");
    }

    public ObjetoNuloException(Throwable t) {
        super(t);
    }
}