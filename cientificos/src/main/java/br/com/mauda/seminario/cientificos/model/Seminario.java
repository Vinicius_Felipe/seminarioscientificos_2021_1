package br.com.mauda.seminario.cientificos.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.seminario.cientificos.exception.ObjetoNuloException;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;

public class Seminario implements DataValidation {

    private Long id;
    private String titulo;
    private String descricao;

    private Boolean mesaRedonda;

    private LocalDate data;

    private Integer qtdInscricoes;

    private List<AreaCientifica> areasCientificas = new ArrayList<>();

    private List<Inscricao> inscricoes = new ArrayList<>();

    private List<Professor> professores = new ArrayList<>();

    public Seminario(AreaCientifica areaCientifica, Professor professor, Integer qtdInscricoes) {
        this.adicionarAreaCientifica(areaCientifica);
        this.adicionarProfessor(professor);
        this.qtdInscricoes = qtdInscricoes;

        for (int i = 0; i < qtdInscricoes; i++) {
            new Inscricao(this);
        }
    }

    public void adicionarAreaCientifica(AreaCientifica areaCientifica) {
        if (Boolean.FALSE.equals(this.possuiAreaCientifica(areaCientifica))) {
            this.areasCientificas.add(areaCientifica);
        }
    }

    public void adicionarInscricao(Inscricao inscricao) {
        this.inscricoes.add(inscricao);
    }

    public void adicionarProfessor(Professor professor) {
        if (Boolean.FALSE.equals(this.possuiProfessor(professor))) {
            this.professores.add(professor);
            professor.adicionarSeminario(this);
        }
    }

    public Boolean possuiAreaCientifica(AreaCientifica area) {
        return this.areasCientificas.contains(area);
    }

    public Boolean possuiInscricao(Inscricao inscricao) {
        return this.inscricoes.contains(inscricao);
    }

    public Boolean possuiProfessor(Professor professor) {
        return this.professores.contains(professor);
    }

    @Override
    public void validateForDataModification() {
        if (this.data == null || this.data.isBefore(LocalDate.now())) {
            throw new SeminariosCientificosException("ER0070");
        }

        if (StringUtils.isBlank(this.descricao) || this.descricao.length() > 200) {
            throw new SeminariosCientificosException("ER0071");
        }

        if (StringUtils.isBlank(this.titulo) || this.titulo.length() > 50) {
            throw new SeminariosCientificosException("ER0072");
        }

        if (this.mesaRedonda == null) {
            throw new SeminariosCientificosException("ER0073");
        }

        if (this.qtdInscricoes == null || this.qtdInscricoes <= 0) {
            throw new SeminariosCientificosException("ER0074");
        }

        this.validarProfessores();
        this.validarAreasCientificas();

    }

    public void validarProfessores() {
        if (this.professores == null || this.professores.isEmpty()) {
            throw new SeminariosCientificosException("ER0075");
        }

        for (Professor prof : this.professores) {
            if (prof == null) {
                throw new ObjetoNuloException();
            }
            prof.validateForDataModification();
        }
    }

    public void validarAreasCientificas() {
        if (this.areasCientificas == null || this.areasCientificas.isEmpty()) {
            throw new SeminariosCientificosException("ER0076");
        }

        for (AreaCientifica areas : this.areasCientificas) {
            if (areas == null) {
                throw new ObjetoNuloException();
            }
            areas.validateForDataModification();
        }
    }

    public Long getId() {
        return this.id;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getMesaRedonda() {
        return this.mesaRedonda;
    }

    public void setMesaRedonda(Boolean mesaRedonda) {
        this.mesaRedonda = mesaRedonda;
    }

    public LocalDate getData() {
        return this.data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public Integer getQtdInscricoes() {
        return this.qtdInscricoes;
    }

    public void setQtdInscricoes(Integer qtdInscricoes) {
        this.qtdInscricoes = qtdInscricoes;
    }

    public List<AreaCientifica> getAreasCientificas() {
        return this.areasCientificas;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

    public List<Professor> getProfessores() {
        return this.professores;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Seminario other = (Seminario) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return true;
    }
}
